# Serverless App Scale

Reference https://catalog.us-east-1.prod.workshops.aws/workshops/b34eab03-4ebe-46c1-bc63-cd2d975d8ad4/en-US

Build, Secure, Manage Serverless Applications at Scale on AWS Workshop!

In this workshop, we will explore creating a serverless application using an API-first design and the AWS Serverless Application Model.

The expected duration to complete this workshop is 4 hours.

The architecture for our ordering service is shown below:
![IMAGE_DESCRIPTION](
https://static.us-east-1.prod.workshops.aws/public/39f3bd8a-235f-4f3c-a23a-e49064f9d420/static/3-orderservice/images/order-arc-v6-cognito.png)
